

#include "mcc_generated_files/mcc.h"
#include "fs_common.h"
#include "timer_multiplex/timer_multiplex.h"
#include "fs_can_lib/fs_can_lib.h"
#include "led_indicators.h"
#include "fs_timing/fs_timing.h"

#include <string.h>


unsigned char tx_disable = 0;
oper_params_t oper_params;
config_params_t config_params;

static void
send_ka(void)
{
    if (!tx_disable) {
        fs_can_send(4,0,0);
    }
}


static void
run_pwm(void)
{
    static unsigned char c = 0;
    c++;
    LATAbits.LATA1 = 1; // Latch

    if (c < oper_params.pwm[0]) {
        LATCbits.LATC5 = 1;
    } else {
        LATCbits.LATC5 = 0;
    }

    LATAbits.LATA1 = 0;
}
static void
data_send(void)
{
    volatile int rc = 0;
    do {
        rc = fs_can_send(5,24, (oper_params.in_bank[0] & 0x01) ? 0 : 1);
    } while (!rc);

    do {
        rc = fs_can_send(5,25, (oper_params.in_bank[0] & 0x02) ? 1 : 0);
    } while (!rc);

    do {
        rc = fs_can_send(5,26, (oper_params.in_bank[0] & 0x04) ? 0 : 1);
    } while (!rc);

    do {
        rc = fs_can_send(5,27, (oper_params.in_bank[1] & 0x01) ? 0 : 1);
    } while (!rc);

    do {
        rc = fs_can_send(5,28, (oper_params.in_bank[1] & 0x02) ? 1 : 0);
    } while (!rc);

    do {
        rc = fs_can_send(5,29, (oper_params.in_bank[1] & 0x04) ? 0 : 1);
    } while (!rc);

    do {
        rc = fs_can_send(5,30, (oper_params.in_bank[2] & 0x01) ? 0 : 1);
    } while (!rc);

    do {
        rc = fs_can_send(5,31, (oper_params.in_bank[2] & 0x02) ? 0 : 1);
    } while (!rc);

    do {
        rc = fs_can_send(5,32, (oper_params.in_bank[2] & 0x04) ? 0 : 1);
    } while (!rc);

    
}

static void
out_bank(unsigned char bank)
{
    unsigned char data = oper_params.out_bank[bank];

    // Last bit of bank1 is PWM
    if (bank != 1) {
        if ( data & 0x80 ) {
            LATCbits.LATC5 = 1;
        } else {
            LATCbits.LATC5 = 0;
        }
    }
    if ( data & 0x40 ) {
        LATCbits.LATC6 = 1;
    } else {
        LATCbits.LATC6 = 0;
    }
    if ( data & 0x20 ) {
        LATCbits.LATC7 = 1;
    } else {
        LATCbits.LATC7 = 0;
    }
    if ( data & 0x10 ) {
        LATBbits.LATB0 = 1;
    } else {
        LATBbits.LATB0 = 0;
    }
    if ( data & 0x08 ) {
        LATBbits.LATB1 = 1;
    } else {
        LATBbits.LATB1 = 0;
    }
    if ( data & 0x04 ) {
        LATCbits.LATC0 = 1;
    } else {
        LATCbits.LATC0 = 0;
    }

    if ( data & 0x02 ) {
        LATCbits.LATC1 = 1;
    } else {
        LATCbits.LATC1 = 0;
    }
    if ( data & 0x01 ) {
        LATCbits.LATC2 = 1;
    } else {
        LATCbits.LATC2 = 0;
    }

    switch(bank) {
        case 0:
            LATAbits.LATA2 = 1;
            break;
        case 1:
            LATAbits.LATA1 = 1;
            break;
    }

    
    
    for(volatile int i=0; i<1000; i++ );
    LATAbits.LATA2 = 0;
    LATAbits.LATA1 = 0;

}
static void
out_channels(void)
{
    static unsigned char bank;
    
    for (bank=0; bank < 2; bank++) {
        out_bank(bank);
    }
    
}
static void
input_scan(void)
{
    unsigned char data = 0;

#define INR0    LATAbits.LATA3
#define INR1    LATBbits.LATB4
#define INR2    LATBbits.LATB5

    TRISAbits.TRISA3 = 1;
    TRISBbits.TRISB4 = 1;
    TRISBbits.TRISB5 = 1;        

    TRISAbits.TRISA3 = 0;
    INR0 = 0;

    data = 0;
    if (PORTAbits.RA5) {
        data |= 0x04;
    }
    if (PORTCbits.RC4) {
        data |= 0x02;
    }
    if (PORTCbits.RC3) {
        data |= 0x01;
    }
    oper_params.in_bank[0] = data;
    TRISAbits.TRISA3 = 1;

    TRISBbits.TRISB4 = 0;
    INR1 = 0;
    data = 0;
    if (PORTAbits.RA5) {
        data |= 0x04;
    }
    if (PORTCbits.RC4) {
        data |= 0x02;
    }
    if (PORTCbits.RC3) {
        data |= 0x01;
    }
    oper_params.in_bank[1] = data;
    TRISBbits.TRISB4 = 1;

    TRISBbits.TRISB5 = 0;
    INR2 = 0;
    data = 0;
    if (PORTAbits.RA5) {
        data |= 0x04;
    }
    if (PORTCbits.RC4) {
        data |= 0x02;
    }
    if (PORTCbits.RC3) {
        data |= 0x01;
    }
    oper_params.in_bank[2] = data;
    TRISBbits.TRISB5 = 1;

}

static void load_default_config(void)
{
    memset(&config_params, 0, sizeof(config_params));
}

static void
save_config(void)
{
    
}

static unsigned char
load_config(void)
{
    unsigned char c;
    if (EEPROM_MAGIC_1_VALUE != DATAEE_ReadByte(EEPROM_MAGIC_1) ||
        EEPROM_MAGIC_2_VALUE != DATAEE_ReadByte(EEPROM_MAGIC_2)){
        return 0;
    }
    return 1;
}

static void
load_default_oper_config(void)
{
    memset(&oper_params, 0, sizeof(oper_params));
}

/*
                         Main application
 */

void ECAN_ISR_ECAN_RXBI(void)
{
    if (PIR5bits.RXB0IF) {
        volatile int a=0;
        rx_can();
        PIR5bits.RXB0IF = 0;

    }
    if (PIR5bits.ERRIF) {
        //tx_disable = 1;
        TXB0CONbits.TXREQ = 0;
        CANCONbits.ABAT = 1;
        COMSTATbits.RXB0OVFL = 0;
        PIR5bits.ERRIF = 0;
        RXB0CONbits.RXFUL = 0;
      
        PIR5 = 0;
        //CANCON = 0x20; // request disable mode
        //while ((CANSTAT & 0xE0) != 0x20); // wait until ECAN is in disable mode   
        //ECAN_Initialize();
        PIR5bits.ERRIF = 0;
    }
    
    
}

void process_chan_pwm(int val, unsigned char idx)
{
    oper_params.pwm[idx] = val; 
}



void process_chan_led(int val, unsigned char bank, unsigned char idx)
{
    unsigned char data = 1;
    while(idx--) {
        data<<=1;
    }
    if (val) {
        oper_params.out_bank[bank] |= data;
    } else {
        oper_params.out_bank[bank] &= ~data;
    }

}
void main(void)
{
    timer_cb_t cb;
    SYSTEM_Initialize();

    volatile int x=0;
    TMR0_SetInterruptHandler(timer_multiplex_cb);
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    
    
    fs_can_init(0x1D, 0x1D);
    

    if (!load_config()) {
        load_default_config();
        save_config();
    }
    load_default_oper_config();
    

    /* KA send */
    cb.every_loops = 1000;
    cb.cb = send_ka;
    timer_add_cb(&cb);
    
    /* Input scan */
    cb.every_loops = 100;
    cb.cb = input_scan;
    timer_add_cb(&cb);

    /* Output write */
    cb.every_loops = 100;
    cb.cb = out_channels;
    timer_add_cb(&cb);

    /* Data send */
    cb.every_loops = 100;
    cb.cb = data_send;
    timer_add_cb(&cb);

    /* PWM */
    TMR2_SetInterruptHandler(run_pwm);

    RX_t Rx;
    PIE5bits.RXB0IE = 1;
    PIE5bits.ERRIE = 1;
    while (1)
    {
        if (fs_can_read(&Rx)) {
            switch(Rx.Cmd) {
                case 1: // CMD
                    switch(Rx.Slot) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            process_chan_led(Rx.Val, 0,Rx.Slot);
                            break;
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                            process_chan_led(Rx.Val, 1,Rx.Slot-8);
                            break;
                        case 15:
                            process_chan_pwm(Rx.Val, 0);
                            break;
                        case 99:
                            RESET();
                            break;
                    }
                    break;
            }
        }
    }
}
/**
 End of File
*/